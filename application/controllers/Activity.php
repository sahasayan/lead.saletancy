<?php

use application\models\LeadModel;
use application\models\UserModel;
use Ramsey\Uuid\Uuid;

class Activity extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }

    function add()
    {
        $new_note_rules = [
            [
                'field' => 'lead_id',
                'label' => 'lead id',
                'rules' => 'trim|required|integer'
            ],
            [
                'field' => 'activity_type',
                'label' => 'activity description',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'activity_description',
                'label' => 'activity description',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'activity_time',
                'label' => 'activity description',
                'rules' => 'trim|required'
            ]
        ];

        $this->form_validation->set_rules($new_note_rules);

        if ($this->form_validation->run() == FALSE) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $user = auth();
        $lead = LeadModel::where('id', $this->input->post('lead_id'))->first();

        try {
            $activity = new \application\models\ActivityModel();
            $activity->lead_id = $lead->id;
            $activity->type = $this->input->post('activity_type');
            $activity->description = $this->input->post('activity_description');
            $activity->time = $this->input->post('activity_time');
            $activity->save();
            event('new_lead_activity', [
                'user' => $user,
                'lead' => $lead,
                'activity' => $activity
            ]);

        } catch (Exception $e) {
            log_message('error', $e->getMessage());

            redirect($_SERVER['HTTP_REFERER']);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
}