<?php

use application\models\UserModel;
use Ramsey\Uuid\Uuid;

class Auth extends CI_Controller
{
    function register()
    {
        // auth check
        if ($this->session->user['username'] !== null) redirect(base_url('user/home'));

        $this->load->view('register');
    }



    function register_post()
    {
        // auth check
        if ($this->session->user['username'] !== null) redirect(base_url('user/home'));

        $this->output->set_content_type('application/json');

        $user_registration_rules = [
            [
                'field' => 'first_name',
                'label' => 'first name',
                'rules' => 'required|alpha|min_length[3]|max_length[255]'
            ],
            [
                'field' => 'last_name',
                'label' => 'last name',
                'rules' => 'required|alpha|min_length[3]|max_length[255]'
            ],
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email|min_length[3]|max_length[255]|is_unique[users.email]',
                'errors' => [
                    'is_unique' => 'Email is already in use.'
                ]
            ],
            [
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required|min_length[8]|max_length[255]'
            ],
            [
                'field' => 'confirm_password',
                'label' => 'confirmation password',
                'rules' => 'required|matches[password]'
            ]
        ];

        $this->form_validation->set_rules($user_registration_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_output(json_encode(array_merge(
                [
                    'success' => false,
                    'is_validation_error' => true
                ],
                [
                    'errors' => $this->form_validation->error_array()
                ]
            )));

            return;
        }

        try {
            $user = new UserModel();
            $user->username = Uuid::uuid4();
            $user->first_name = $this->input->post('first_name');
            $user->last_name = $this->input->post('last_name');
            $user->email = $this->input->post('email');
            $user->password_raw = $this->input->post('password');
            $user->password = md5($this->input->post('password'));
            $user->save();

        } catch (Exception $e) {
            log_message('error', $e->getMessage());

            $this->output->set_output(json_encode([
                'success' => false,
                'message' => 'Something went wrong'
            ]));

            return;
        }

        $this->output->set_output(json_encode([
            'success' => true,
        ]));
    }



    function login()
    {
        // auto login - temporary
        $this->session->user = [
            'username' => UserModel::all()->first()->username
        ];
        
        // auth check
        if ($this->session->user['username'] !== null) redirect(base_url('user/home'));

        $this->load->view('login');
    }



    function login_post()
    {
        // auth check
        if ($this->session->user['username'] !== null) redirect(base_url('user/home'));

        $this->output->set_content_type('application/json');

        $user_registration_rules = [
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email|min_length[3]|max_length[255]'
            ],
            [
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required|min_length[8]|max_length[255]'
            ]
        ];

        $this->form_validation->set_rules($user_registration_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_output(json_encode(array_merge(
                [
                    'success' => false,
                    'is_validation_error' => true
                ],
                [
                    'errors' => $this->form_validation->error_array()
                ]
            )));

            return;
        }

        $user = UserModel::where([
            'email' =>  $this->input->post('email'),
            'password' =>  md5($this->input->post('password'))
        ])->first();

        if (empty($user)) {
            $this->output->set_output(json_encode([
                'success' => false,
                'message' => 'Wrong credentials'
            ]));

            return;
        }

        $this->session->user['username'] = $user->username;

        $this->output->set_output(json_encode([
            'success' => true,
            'redirect_to' => base_url('user/home')
        ]));
    }

    function logout()
    {
        $this->session->unset_userdata('user');
    }
}