<?php

use application\models\AutomationModel;
use application\models\LeadModel;
use application\models\UserModel;
use Ramsey\Uuid\Uuid;

class Automation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }



    public function index()
    {
        $user = auth();

        $user->load('automations');

        $this->load->view('user/automations', [
            'user' => $user
        ]);
    }

    public function create()
    {
        $this->load->view('user/automation-create');
    }



    public function add()
    {
        $user = auth();

        try {
            $automation = new AutomationModel();
            $automation->uuid = Uuid::uuid4();
            $automation->user_id = $user->id;
            $automation->event = $this->input->post('automation_event');
            $automation->action = $this->input->post('automation_action');
            $automation->message = $this->input->post('automation_message');
            $automation->save();
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
        }

        redirect(base_url('automation'));
    }



    public function test()
    {
//        $email = new \SendGrid\Mail\Mail();
//        $email->setFrom("example@example.com", "Example User");
//        $email->setSubject("Sending with SendGrid is Fun");
//        $email->addTo("vetoru@yk20.com", "Example User");
//        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
//        $email->addContent(
//            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
//        );
//        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
//        try {
//            $response = $sendgrid->send($email);
//            dd($response);
//        } catch (Exception $e) {
//            log_message('error', $e->getMessage());
//        }


//        event('new_lead', [
//            'name' => 'sayan'
//        ]);


//        $msg91 = new \lucky\Msg91\Msg91(env('MSG91_API_KEY'));
//        $mobiles = "919007486149"; // mobile no. to whom you want to send sms. Including country code.
//        $message = " your text sms will come up here"; // Message content to send
//        $sender = "MSGIND"; // Receiver will see this as sender's ID.
//        $route = "4"; // Route you want to use. 1 for promotional route and 4 for transactional route
//        $country = "91"; //numeric	0 for international,1 for USA, 91 for India.
//
//        $data = array( "authkey" => env('MSG91_API_KEY'),
//            "mobiles" => $mobiles,
//            "message" => $message,
//            "sender" => $sender,
//            "route" => $route,
//            "country" => $country,
//        );
//
//        $output = $msg91->sendSMS($data);
//        dd($output);
    }
}