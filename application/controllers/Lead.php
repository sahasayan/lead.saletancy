<?php

use application\models\LeadModel;
use application\models\LeadFieldModel;
use Ramsey\Uuid\Uuid;

class Lead extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }



    public function index()
    {
        $user = auth();
        $leads = LeadModel::where('user_id', $user->id)->get()->toArray();

        foreach ($leads as $index => $lead) {
            $lead_data = json_decode($lead['data'], true);
            $leads[$index] = array_merge($leads[$index], $lead_data);
            unset($leads[$index]['id'], $leads[$index]['user_id'], $leads[$index]['data']);
        }

        //dd($leads);

        //
        $additional_lead_fields = LeadFieldModel::getAdditionalFieldsByUserId($user->id);
        $additional_lead_columns = [];

        foreach ($additional_lead_fields as $index => $additional_lead_field) {
            $additional_lead_columns[$index]['field'] = $additional_lead_field->code;
            $additional_lead_columns[$index]['title'] = $additional_lead_field->name;
            $additional_lead_columns[$index]['width'] = 200;
        }

        // dd($additional_lead_columns);

        $this->load->view('user/leads', [
            'leads' => $leads,
            'additional_lead_columns' => $additional_lead_columns,
            'lead_fields' => LeadFieldModel::getAllFieldsByUserId($user->id)
        ]);
    }



    public function view($uuid = null)
    {
        $user = auth();

        $lead = LeadModel::where('uuid', $uuid)->Where('user_id', $user->id)->first()->toArray();
        abort_if($lead === null);
        $lead_fields = LeadFieldModel::getAllFieldsByUserId($user->id);

        $lead_data = json_decode($lead['data'], true);
        $lead = array_merge($lead, $lead_data);

        $this->load->view('user/lead', [
            'lead' => $lead,
            'lead_fields' => $lead_fields
        ]);
    }



    public function add()
    {
        $this->output->set_content_type('application/json');

        $user = auth();
        $lead_fields = LeadFieldModel::getAllFieldsByUserId($user->id);
        $new_lead_rules = $lead_data = [];

        // Generate data validation rules
        //
        foreach ($lead_fields as $index => $lead_field) {
            $new_lead_rules[$index]['field'] = $lead_field->code;
            $new_lead_rules[$index]['label'] = $lead_field->name;
            $new_lead_rules[$index]['rules'] = 'trim';

            // check if required
            //
            if ($lead_field->required) {
                $new_lead_rules[$index]['rules'] .= '|required';
            }

            // lead field type checking
            //
            if ($lead_field->type === 'email') {
                $new_lead_rules[$index]['rules'] .= '|valid_email|max_length[255]';

            } else if ($lead_field->type === 'website') {
                $new_lead_rules[$index]['rules'] .= '|valid_url|max_length[255]';

            } else if ($lead_field->type === 'text') {
                $new_lead_rules[$index]['rules'] .= ('|max_length[' . $lead_field->max_length . ']');

            } else if ($lead_field->type === 'number') {
                $new_lead_rules[$index]['rules'] .= '|integer';

            } else if ($lead_field->type === 'dropdown') {
                $new_lead_rules[$index]['rules'] .= ('|in_list' . str_replace(['"', ' '], '', $lead_field->values));
            }
        }

        //die(json_encode($new_lead_rules));

        // Data validation
        //
        $this->form_validation->set_rules($new_lead_rules);
        if ($this->form_validation->run() == FALSE) {
            $this->output->set_output(json_encode(array_merge(
                [
                    'success' => false,
                    'is_validation_error' => true
                ],
                [
                    'errors' => $this->form_validation->error_array()
                ]
            )));

            return;
        }

        // Prepare lead data
        //
        foreach ($lead_fields as $lead_field) {
            $lead_data[$lead_field->code] = $this->input->post($lead_field->code);
        }

        //die(json_encode($lead_data));

        // Save in DB
        //
        try {
            $lead = new LeadModel();
            $lead->uuid = Uuid::uuid4();
            $lead->user_id = $user->id;
            $lead->data = json_encode($lead_data);
            $lead->save();
            event('new_lead', [
                'user' => $user,
                'lead' => $lead
            ]);

        } catch (Exception $e) {
            log_message('error', $e->getMessage());

            $this->output->set_output(json_encode([
                'success' => false,
                'message' => 'Something went wrong'
            ]));

            return;
        }

        $this->output->set_output(json_encode([
            'success' => true,
        ]));
    }


    function add_csv()
    {
        $config = [
            'upload_path' => __DIR__ . '/../../uploads',
            'allowed_types' => 'txt|csv',
            'file_name' => Uuid::uuid4() . '.csv',
            'overwrite' => false,
            'max_size' => 2048000, // Can be set to particular file size , here it is 2 MB(2048 Kb)
        ];

        $this->load->library('upload', $config);

        if($this->upload->do_upload('leads_csv')) {
            $data = array('upload_data' => $this->upload->data());

        } else {
            $data = array('error' => $this->upload->display_errors());
        }

        $user = auth();
        $lead_data = csvToArray($data['upload_data']['full_path']);
        foreach ($lead_data as $temp) {
            $lead = new LeadModel();
            $lead->user_id = $user->id;
            $lead->data = json_encode($temp);
            $lead->save();
        }

        redirect(base_url('/lead'));
    }



    public function remove($uuid = [])
    {
        $user = auth();
        $lead = LeadModel::where('uuid', $uuid)->where('user_id', $user->id)->first();
        abort_if($lead == null);

        $lead->delete();
        event('delete_lead', [
            'user' => $user,
            'lead' => $lead
        ]);
        redirect(base_url('lead'));
    }



    function fields()
    {
        $user = auth();

        $this->load->view('user/lead-fields', [
            'lead_fields' => LeadFieldModel::getAllFieldsByUserId($user->id)
        ]);
    }

    function field_add()
    {
        $user = auth();
        try {
            $leadFieldModel = new LeadFieldModel();
            $leadFieldModel->user_id = $user->id;
            $leadFieldModel->code = snake_case($this->input->post('field_name'));
            $leadFieldModel->name = $this->input->post('field_name');
            $leadFieldModel->required = $this->input->post('field_is_required');
            $leadFieldModel->max_length = 255;

            if (array_search($this->input->post('field_type'), ['text', 'email', 'website', 'number',])) {
                $leadFieldModel->type = $this->input->post('field_type');
                $leadFieldModel->show_as = $this->input->post('field_show_as');

            } elseif ($this->input->post('field_type') === 'dropdown') {
                $leadFieldModel->type = 'dropdown';
                $leadFieldModel->show_as = 'dropdown';
                $leadFieldModel->values = json_encode(explode(',', $this->input->post('field_values')));
            }
            $leadFieldModel->save();

        } catch (Exception $e) {
            log_message('error', $e->getMessage());
        }

        $this->load->view('user/lead-fields', [
            'lead_fields' => LeadFieldModel::getAllFieldsByUserId($user->id)
        ]);
    }
}