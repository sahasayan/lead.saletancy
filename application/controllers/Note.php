<?php

use application\models\LeadModel;
use application\models\UserModel;
use Ramsey\Uuid\Uuid;

class Note extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }

    function add()
    {
        $new_note_rules = [
            [
                'field' => 'lead_id',
                'label' => 'lead id',
                'rules' => 'trim|required|integer'
            ],
            [
                'field' => 'note_description',
                'label' => 'note description',
                'rules' => 'trim|required'
            ]
        ];

        $this->form_validation->set_rules($new_note_rules);

        if ($this->form_validation->run() == FALSE) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $user = auth();
        $lead = LeadModel::where('id', $this->input->post('lead_id'))->first();

        try {
            $note = new \application\models\NoteModel();
            $note->lead_id = $lead->id;
            $note->description = $this->input->post('note_description');
            $note->save();
            event('new_lead_note', [
                'user' => $user,
                'lead' => $lead,
                'note' => $note
            ]);

        } catch (Exception $e) {
            log_message('error', $e->getMessage());

            redirect($_SERVER['HTTP_REFERER']);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
}