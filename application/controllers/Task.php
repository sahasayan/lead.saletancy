<?php

use application\models\LeadModel;
use application\models\UserModel;
use Ramsey\Uuid\Uuid;

class Task extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }

    function add()
    {
        $new_task_rules = [
            [
                'field' => 'lead_id',
                'label' => 'lead id',
                'rules' => 'trim|required|integer'
            ],
            [
                'field' => 'task_description',
                'label' => 'task description',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'task_schedule',
                'label' => 'task schedule',
                'rules' => 'trim|required'
            ]
        ];

        $this->form_validation->set_rules($new_task_rules);

        if ($this->form_validation->run() == FALSE) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $user = auth();
        $lead = LeadModel::where('id', $this->input->post('lead_id'))->first();

        try {
            $task = new \application\models\TaskModel();
            $task->lead_id = $lead->id;
            $task->description = $this->input->post('task_description');
            $task->schedule = $this->input->post('task_schedule');
            $task->save();
            event('new_lead_task', [
                'user' => $user,
                'lead' => $lead,
                'task' => $task
            ]);

        } catch (Exception $e) {
            log_message('error', $e->getMessage());

            redirect($_SERVER['HTTP_REFERER']);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
}