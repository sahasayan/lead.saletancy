<?php

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // auth check
        if ($this->session->user['username'] === null) {
            redirect(base_url('auth/login'));
        }
    }

    public function home()
    {
        $this->load->view('user/home');
    }
}
