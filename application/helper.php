<?php

function auth($type = 'user')
{
    return ($_SESSION[$type]['username']) ? \application\models\UserModel::where('username', $_SESSION[$type]['username'])->first() : null;
}

function abort_if($condition = true)
{
    if ($condition) {
        redirect(env('BASE_URL') .  '/404');
    }
}

function app_mail($to, $to_name, $from, $from_name, $subject, $message)
{
    $email = new \SendGrid\Mail\Mail();
    $email->setFrom($from, $from_name);
    $email->setSubject($subject);
    $email->addTo($to, $to_name);
    //$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
    $email->addContent("text/html", $message);
    $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
    try {
        $response = $sendgrid->send($email);
    } catch (Exception $e) {
        log_message('error', $e->getMessage());
    }
}

function csvToArray($filepath = '', $delimiter = ',')
{

    if (!file_exists($filepath) || !is_readable($filepath)) {
        return false;
    }

    $header = null;
    $data = [];

    if (($handle = fopen($filepath, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header) {
                $header = trim($header);
                $header = $row;
            }
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}