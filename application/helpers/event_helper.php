<?php

/**
 * @param $event_name
 * @param array $data
 * @return bool
 */
function event($event_name, $data = [])
{
    // list of all events and listeners
    $events = [
        'new_lead' => \application\listeners\NewLeadListener::class,
        'new_lead_activity' => \application\listeners\NewLeadActivityListener::class,
        'new_lead_task' => \application\listeners\NewLeadTaskListener::class,
        'new_lead_note' => \application\listeners\NewLeadNoteListener::class,
        'update_lead' => \application\listeners\UpdateLeadListener::class,
        'delete_lead' => \application\listeners\DeleteLeadListener::class
    ];

    if (array_key_exists($event_name, $events)) {
        $emitter = new \League\Event\Emitter();
        $emitter->addListener($event_name, new $events[$event_name]());
        $emitter->emit($event_name, $data);
        return true;

    } else {
        log_message('error', $event_name . ', wrong event name');
        return false;
    }
}