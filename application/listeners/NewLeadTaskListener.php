<?php

namespace application\listeners;

use application\models\AutomationModel;
use League\Event\ListenerInterface;
use League\Event\EventInterface;

class NewLeadTaskListener implements ListenerInterface
{
    public function isListener($listener)
    {
        return $listener === $this;
    }

    public function handle(EventInterface $event, $data = null)
    {
        $user = $data['user'];
        $task = $data['task'];
        $lead = $data['lead']->toArray();
        $lead = array_merge($lead, json_decode($lead['data'], true));

        $automations = AutomationModel::where('user_id', $user->id)->where('event', 'new_lead_task')->get();

        foreach ($automations as $index => $automation) {
            if ($automation->action == 'mail') {
                app_mail($lead['email'], '', $user->email, '', 'subject', $automation->message);

            } elseif ($automation->action == 'sms') {

            }
        }
    }
}