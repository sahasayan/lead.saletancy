<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ActivityModel extends Eloquent {
    protected $table = 'activities';
}