<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class AutomationModel extends Eloquent {
    protected $table = 'automations';
}