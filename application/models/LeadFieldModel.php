<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LeadFieldModel extends Eloquent {
    protected $table = 'lead_fields';

    /**
     * Returns all lead fields of a user
     *
     * @param int $user_id
     * @return null | []
     */
    static function getAllFieldsByUserId($user_id = null)
    {
        if ($user_id === null) return $user_id;

        return LeadFieldModel::where('user_id', null)->orWhere('user_id', $user_id)->get();
    }

    /**
     * Returns additional fields of a lead for a user
     *
     * @param int $user_id
     * @return null | []
     */
    static function getAdditionalFieldsByUserId($user_id = null)
    {
        if ($user_id === null) return $user_id;

        return LeadFieldModel::where('user_id', $user_id)->get();
    }
}