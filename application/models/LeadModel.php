<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LeadModel extends Eloquent {
    protected $table = 'leads';

    function lead_stage()
    {
        return $this->belongsTo(LeadStageModel::class);
    }

    function tasks()
    {
        return $this->hasMany(TaskModel::class);
    }

    function notes()
    {
        return $this->hasMany(NoteModel::class);
    }

    function activities()
    {
        return $this->hasMany(ActivityModel::class);
    }
}