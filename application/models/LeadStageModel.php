<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LeadStageModel extends Eloquent {
    protected $table = '';

    public static function getAll()
    {
        $a = LeadFieldModel::where('code', 'lead_stage')->get(['values'])->first()->toArray();
        $a = json_decode($a['values'], true);
        return $a;
    }
}