<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class NoteModel extends Eloquent {
    protected $table = 'notes';
}