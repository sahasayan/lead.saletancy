<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TaskModel extends Eloquent {
    protected $table = 'tasks';
}