<?php

namespace application\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserModel extends Eloquent {
    protected $table = 'users';

    function automations()
    {
        return $this->hasMany(AutomationModel::class, 'user_id', 'id');
    }
}