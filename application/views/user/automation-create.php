<html lang="en">
<head>
    <title>Create Automation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
    <style>
        .text-style-1 {
            font-size: 15px;
            font-weight: 600;
            color: gray;
            padding: 0 15px 0 0;
        }
    </style>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container">
        <div class="ui segment" style="margin-top: 100px; text-align: center">
            <div class="row" id="automation-event">
                <div class="col-sm-12">
                    <span class="text-style-1">Select an event</span>
                    <select class="ui search dropdown" name="automation_event">
                        <option value="">Select</option>
                        <option value="new_lead">New Lead</option>
                        <option value="update_lead">Update Lead</option>
                        <option value="delete_lead">Delete Lead</option>
                        <option value="new_lead_activity">New Activity</option>
                        <option value="new_lead_task">New Task</option>
                        <option value="new_lead_note">New Note</option>
                    </select>
                </div>
            </div>

            <div class="row" style="margin-top: 35px;" id="automation-action">
                <div class="col-sm-12">
                    <span class="text-style-1">Select an action</span>
                    <select class="ui search dropdown" name="automation_action">
                        <option value="">Select</option>
                        <option value="mail">Mail</option>
                        <option value="sms">SMS</option>
                    </select>
                </div>
            </div>

            <div class="row" id="automation-message" style="margin-top: 45px;">
                <div class="col-sm-12">
                    <div class="ui form">
                        <span class="text-style-1">Write your message</span><br><br>
                        <textarea name="" id="" cols="30" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="row" id="automation-submit">
                <div class="col-sm-12" style="text-align: right;">
                    <button type="submit" class="ui green button" tabindex="0" name="add_automation">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </main>

    <?php include_once __DIR__ . '/include/footer.php'?>
    <?php include_once __DIR__ . '/include/scripts.php'?>
    <script>
        $('.ui.dropdown').dropdown();
        $('#automation-action, #automation-message, [name=add_automation]').hide();
        $('#automation-event').on('change', function () {
            $('#automation-action').show();
        });
        $('#automation-action').on('change', function () {
            $('#automation-message, [name=add_automation]').show();
        });
        $('[name=add_automation]').on('click',function () {
            post_to_url('/automation/add', {
                'automation_event': $('[name=automation_event]').val(),
                'automation_action': $('[name=automation_action]').val(),
                'automation_message': $('textarea').val()
            });
        });


    </script>
</body>
