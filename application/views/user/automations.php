<html lang="en">
<head>
    <title>Automations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container">
        <div class="row" style="padding: 40px 0 25px 0">
            <div class="col-sm-12">
                <a class="ui right labeled icon green medium button" id="btn-create-automation" href="/automation/create">
                    <i class="external alternate icon"></i>
                    Create Automation
                </a>
            </div>
        </div>
        <div>
            <table class="ui celled table datatable" style="width: 100%;">
                <thead>
                    <tr class="center aligned">
                        <th><i class="icon hashtag"></i></th>
                        <th>Name</th>
                        <th>Action</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($user->automations as $index => $automation): ?>
                        <tr>
                            <td class="center aligned"><?= $index + 1 ?></td>
                            <td class="center aligned"><?= $automation->event ?></td>
                            <td class="center aligned"><?= $automation->action ?></td>
                            <td class="center aligned"><?= $automation->created_at ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

    <?php include_once __DIR__ . '/include/footer.php'?>
    <?php include_once __DIR__ . '/include/scripts.php'?>
    <script>

    </script>
</body>
