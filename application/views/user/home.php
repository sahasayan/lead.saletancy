<html lang="en">
<head>
    <title>Lead</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container" style="margin-top: 150px">
        <p style="text-align: center; font-size: 16px; color: #616161;font-weight: 600;">Welcome to Saletancy Pipeline portal, head over to <a href="/lead">lead</a> section to manage your contacts
        . You can also setup <a href="/automation">automation</a>.</p>
        <br>

    </main>

    <?php include_once __DIR__ . '/include/footer.php'?>
    <?php include_once __DIR__ . '/include/scripts.php'?>
</body>
