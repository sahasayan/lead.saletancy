<header>
    <div class="ui container">
        <div id="head-icon"><a href="/user/home"><img src="http://res.cloudinary.com/saletancy-com/image/upload/v1528589636/saletancy_logo_bzl8d1.png" alt=""></a></div>
        <div id="header-dropdown" class="ui right dropdown item" style="position: absolute; top: 50%; right: 10%; transform: translateY(-50%); color: #616161;font-weight: 600;">
            Menu
            <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item" href="/user/home">Home</a>
                <a class="item" href="/lead">Leads</a>
                <a class="item" href="/lead/fields">Lead fields</a>
                <a class="item" href="/automation">Automation</a>
                <a class="item" href="/logout">Logout</a>

            </div>
        </div>
    </div>
</header>