<html lang="en">
<head>
    <title>Manage Lead Fields</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container">
        <div class="row" style="padding: 40px 0 25px 0">
            <div class="col-sm-12">
                <button class="ui labeled icon green medium button" id="add-lead-field">
                    <i class="expand arrows alternate icon"></i>
                    Add Lead Field
                </button>
            </div>
        </div>
        <div>
            <table class="ui celled table datatable" style="width: 100%;">
                <thead>
                    <tr class="center aligned">
                        <th><i class="icon hashtag"></i></th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Show As</th>
                        <th>Required</th>
                        <th>Max Length</th>
                        <th>Values</th>
                        <th>Created By</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lead_fields as $index => $lead_field): ?>
                        <tr>
                            <td class="center aligned"><?= $index + 1 ?></td>
                            <td><?= $lead_field->name ?></td>
                            <td class="center aligned"><?= $lead_field->type ?></td>
                            <td class="center aligned"><?= $lead_field->show_as ?></td>
                            <td class="center aligned">
                                <?= ($lead_field->required) ? '<i class="icon green circle"></i>' : '<i class="icon red circle"></i>' ?>
                            </td>
                            <td class="center aligned"><?= $lead_field->max_length ?></td>
                            <td class="center aligned">
                                <?php $lead_field->values = json_decode($lead_field->values); ?>
                                <?php if (is_array($lead_field->values)): ?>
                                    <div class="ui compact menu">
                                        <div class="ui simple dropdown item">
                                            view
                                            <i class="dropdown icon"></i>
                                            <div class="menu">
                                                <?php foreach ($lead_field->values as $lead_stage): ?>
                                                    <div class="item"><?= $lead_stage ?></div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php else:  ?>
                                    <span></span>
                                <?php endif; ?>
                            </td>
                            <td class="center aligned">
                                <?= ($lead_field->user_id == null) ? 'system' : 'you' ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

    <div class="ui medium modal" id="add-lead-field-modal">
        <i class="close icon"></i>
        <div class="header">Add New Field</div>
        <div class="scrolling content">
            <form class="ui form" action="/lead/field_add" method="POST">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Name</label>
                            <input type="text" placeholder="Enter Field Name" name="field_name" required>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Value is required?</label>
                            <select class="ui search dropdown simple-dropdown" name="field_is_required" required>
                                <option value="">Select if field value is required</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Type</label>
                            <select class="ui search dropdown simple-dropdown" name="field_type">
                                <option value="">Select field type</option>
                                <option value="text">Text</option>
                                <option value="email">Email</option>
                                <option value="website">Website</option>
                                <option value="number">Number</option>
                                <option value="dropdown">Dropdown</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row pt-10" style="display: none">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Show as</label>
                            <select class="ui search dropdown simple-dropdown" name="field_show_as">
                                <option value="">Select the input box</option>
                                <option value="textbox">Textbox</option>
                                <option value="textarea">Textarea</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row pt-10" style="display: none">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Values<span>(use comma to separate values)</span></label>
                            <div class="ui fluid multiple search selection dropdown">
                                <input name="field_values" type="hidden">
                                <div class="default text">Enter dropdown values</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="field">
                            <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_lead_field">
                                <i class="plus icon"></i>
                                Add
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php include_once __DIR__ . '/include/footer.php'?>
    <?php include_once __DIR__ . '/include/scripts.php'?>
    <script src="/assets/js/user.lead-field.js"></script>
</body>