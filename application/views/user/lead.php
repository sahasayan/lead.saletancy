<html lang="en">
<head>
    <title>Lead</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container">
        <div class="row" style="padding: 50px 0 0 0">
            <div class="col-sm-3">
                <div class="ui raised segment">
                    <div class="ui celled list">
                        <?php foreach ($lead_fields as $lead_field): ?>
                            <div class="item">
                                <div class="content">
                                    <a class="header"><?= $lead_field->name ?></a>
                                    <div class="description">
                                        <?php if(isset($lead[$lead_field->code]) && !empty($lead[$lead_field->code])): ?>
                                            <?= $lead[$lead_field->code] ?>
                                        <?php else:  ?>
                                            N\A
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="ui raised segment">
                    <div class="ui basic buttons">
                        <button class="ui button" id="add-activity">
                            <i class="history icon"></i>
                            Add Activity
                        </button>
                        <button class="ui button" id="add-task">
                            <i class="tasks icon"></i>
                            Add Task
                        </button>
                        <button class="ui button" id="add-note">
                            <i class="sticky note icon"></i>
                            Add Note
                        </button>
                    </div>
                    <hr style="border: 1px solid #868686">

                    <div class="ui top attached tabular menu">
                        <a class="item active" data-tab="activities">Activity History</a>
                        <a class="item" data-tab="tasks">Tasks</a>
                        <a class="item" data-tab="notes">Notes</a>
                        <a class="item" data-tab="lead_action">Lead Action</a>
                    </div>
                    <div class="ui bottom attached tab segment active" data-tab="activities">
                        <div class="ui small feed">
                            <?php
                                $activities = \application\models\ActivityModel::where('lead_id', $lead['id'])->get();
                            ?>
                            <?php foreach ($activities as $activity): ?>
                                <div class="event">
                                    <div class="label">
                                        <i class="history icon"></i>
                                    </div>
                                    <div class="content">
                                        <div class="summary"><?php echo $activity->type ?></div>
                                        <?php echo $activity->description; ?>
                                        <br><br>
                                        <div class="date"><?php echo $activity->time; ?></div>
                                    </div>
                                </div>
                                <hr style="border: 0.5px solid #E0E0E0">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="ui bottom attached tab segment" data-tab="tasks">
                        <div class="ui small feed">
                            <?php
                                $tasks = \application\models\TaskModel::where('lead_id', $lead['id'])->get();
                            ?>
                            <?php foreach ($tasks as $task): ?>
                                <div class="event">
                                    <div class="label">
                                        <i class="thumbtack icon"></i>
                                    </div>
                                    <div class="content">
                                        <div class="summary"><?php echo $task->schedule ?></div>
                                        <?php echo $task->description; ?>
                                        <br><br>
                                        <div class="date"><?php echo $task->created_at; ?></div>
                                    </div>
                                </div>
                                <hr style="border: 0.5px solid #E0E0E0">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="ui bottom attached tab segment" data-tab="notes">
                        <div class="ui small feed">
                            <?php
                                $notes = \application\models\NoteModel::where('lead_id', $lead['id'])->get();
                            ?>
                            <?php foreach ($notes as $note): ?>
                                <div class="event">
                                    <div class="label">
                                        <i class="icon sticky note outline"></i>
                                    </div>
                                    <div class="content">
                                        <?php echo $note->description; ?>
                                        <br><br>
                                        <div class="date"><?php echo $note->created_at; ?></div>
                                    </div>
                                </div>
                                <hr style="border: 0.5px solid #E0E0E0">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="ui bottom attached tab segment" data-tab="lead_action">
                        <a class="ui right labeled icon red basic button" href="/lead/remove/<?= $lead['uuid'] ?>">
                            <i class="red trash icon"></i>
                            Delete
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once __DIR__ . '/include/footer.php'?>

    <!-- start of modal to add activity -->
    <div class="ui modal" id="add-activity-modal">
        <i class="close icon"></i>
        <div class="header">Add Activity</div>
        <div class="scrolling content">
            <form class="ui form" action="/activity/add" method="post">
                <input type="hidden" name="lead_id" value="<?= $lead['id'] ?>">
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Type</label>
                            <input type="text" name="activity_type" required>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Description</label>
                            <textarea rows="3" name="activity_description" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12 col-md-6">
                        <div class="required field">
                            <label>Time</label>
                            <input type="datetime-local" name="activity_time" required>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12 col-md-6">
                        <div class="field">
                            <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_activity">
                                <i class="plus icon"></i>
                                Add
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- end of modal to add activity -->


    <!-- start of modal to add task -->
    <div class="ui modal" id="add-task-modal">
        <i class="close icon"></i>
        <div class="header">Add Task</div>
        <div class="scrolling content">
            <form class="ui form" action="/task/add" method="post">
                <input type="hidden" name="lead_id" value="<?= $lead['id'] ?>">
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Description</label>
                            <textarea rows="3" name="task_description" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12 col-md-6">
                        <div class="required field">
                            <label>Schedule</label>
                            <input type="datetime-local" name="task_schedule" required>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12 col-md-6">
                        <div class="field">
                            <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_task">
                                <i class="plus icon"></i>
                                Add
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- end of modal to add task -->


    <!-- start of modal to add note -->
    <div class="ui modal" id="add-note-modal">
        <i class="close icon"></i>
        <div class="header">Add Note</div>
        <div class="scrolling content">
            <form class="ui form" action="/note/add" method="post">
                <input type="hidden" name="lead_id" value="<?= $lead['id'] ?>">
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="required field">
                            <label>Description</label>
                            <textarea rows="3" name="note_description" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row pt-10">
                    <div class="col-sm-12">
                        <div class="field">
                            <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_note">
                                <i class="plus icon"></i>
                                Add
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- end of modal to add note -->
</body>
</html>

<?php include_once __DIR__ . '/include/scripts.php'?>
<script src="/assets/js/user.lead.js"></script>
