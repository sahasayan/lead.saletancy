<html lang="en">
<head>
    <title>Leads</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include_once __DIR__ . '/include/styles.php' ?>
    <style>
        div.k-header.k-grid-toolbar {
            border-color: #59bb46;
        }
        div.k-header {
            background-color: #59bb46;
        }
        .k-grid .k-header .k-button {
            background-color: #55b342;
            border-color: #55b342;
        }
        .k-grid .k-header .k-button:hover {
            background-color: #59bb46;
            border-color: #55b342;
        }
    </style>
</head>
<body>
    <?php include_once __DIR__ . '/include/header.php'?>

    <main class="ui container">
        <div class="row" style="padding: 40px 0 25px 0">
            <div class="col-sm-12">
                <button class="ui labeled icon green medium button" id="add-new-lead">
                    <i class="expand arrows alternate icon"></i>
                    Add Lead
                </button>
                <a class="ui right labeled icon green medium button" id="add-new-lead-field" href="/lead/fields">
                    <i class="external alternate icon"></i>
                    Manage Lead Field
                </a>
            </div>
        </div>
        <div id="leads"></div>
    </main>

    <?php include_once __DIR__ . '/include/footer.php'?>

    <div class="ui large modal" id="add-lead-modal">
        <i class="close icon"></i>
        <div class="header">Add New Lead</div>
        <div class="scrolling content">
            <div class="ui top attached tabular menu">
                <a class="item active" data-tab="single-lead">Single Lead</a>
                <a class="item" data-tab="batch-lead">Batch Leads</a>
            </div>
            <div class="ui bottom attached tab segment active" data-tab="single-lead">
                <form class="ui form" action="javascript:void(0)">

                    <div class="row">
                        <?php foreach ($lead_fields as $lead_field): ?>
                            <div class="col-sm-12 <?= ($lead_field->show_as != 'textarea') ? 'col-md-6' : '' ?> pt-10">
                                <div class="<?= ($lead_field->required) ? 'required' : '' ?> field">
                                    <label><?= $lead_field->name ?></label>

                                    <!-- Lead Field : text, email, website, number -->
                                    <?php if ($lead_field->type == 'text' || $lead_field->type == 'email' || $lead_field->type == 'website' || $lead_field->type == 'number'): ?>

                                        <!-- Show as textbox --->
                                        <?php if ($lead_field->show_as == 'textbox'): ?>
                                            <input type="text" placeholder="<?= $lead_field->name ?>" name="<?= $lead_field->code ?>">

                                        <!-- Show as textarea -->
                                        <?php elseif ($lead_field->show_as == 'textarea'): ?>
                                            <textarea rows="2" name="<?= $lead_field->code ?>"></textarea>
                                        <?php endif; ?>

                                    <!-- Lead Field : dropdown -->
                                    <?php elseif ($lead_field->type == 'dropdown'): ?>
                                        <select class="ui search dropdown" name="<?= $lead_field->code ?>">
                                            <option value="">Select <?= $lead_field->name ?></option>
                                            <?php $lead_field->values = json_decode($lead_field->values) ?>
                                            <?php foreach ($lead_field->values as $lead_field_value): ?>
                                                <option value="<?= $lead_field_value ?>"><?= $lead_field_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php endif; ?>

                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row pt-10">
                        <div class="col-sm-12">
                            <div class="field">
                                <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_lead">
                                    <i class="plus icon"></i>
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="batch-lead">
                <form action="/lead/add_csv" method="post" enctype="multipart/form-data">
                    <div class="row pt-10">
                        <div class="col-sm-12">
                            <div class="field">
                                <div class="ui action input">
                                    <input type="text" placeholder="File 1" readonly>
                                    <input type="file" style="display: none;" name="leads_csv">
                                    <div class="ui icon button">
                                        <i class="attach icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-10">
                        <div class="col-sm-12">
                            <div class="field">
                                <button type="submit" class="ui labeled icon green button" tabindex="0" name="add_csv_lead">
                                    <i class="cloud upload icon"></i>
                                    Upload
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-sm-12">
                        <h4>available lead fields</h4>
                        <ul>
                            <?php foreach ($lead_fields as $lead_field): ?>
                                <li><?= $lead_field->code ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<?php include_once __DIR__ . '/include/scripts.php'?>
<script>
    $(document).ready(function () {
        $('.ui.dropdown').dropdown();
    });
    window.leads = <?= json_encode($leads) ?>;
    window.lead_columns = [
        {
            field: 'uuid',
            title: '.',
            width: 70,
            template: '<a href="/lead/view/${uuid}"><i class="icon external alternate"></i></a>',
            filterable: false,
            sortable: false,
            locked: true
        },
        {
            field: 'first_name',
            title: 'First Name',
            width: 200
        },
        {
            field: 'last_name',
            title: 'Last Name',
            width: 200
        },
        {
            field: 'lead_stage',
            title: 'Lead Stage',
            width: 200
        },
        {
            field: 'address',
            title: 'Address',
            width: 200
        },
        {
            field: 'mobile_number',
            title: 'Mobile',
            width: 200
        },
        {
            field: 'company',
            title: 'Company',
            width: 200
        },
        {
            field: 'website',
            title: 'Website',
            width: 200
        },
        {
            field: 'email',
            title: 'Email',
            width: 200
        },
        {
            field: 'city',
            title: 'City',
            width: 200
        },
        {
            field: 'state',
            title: 'State',
            width: 200
        },
        {
            field: 'country',
            title: 'Country',
            width: 200
        },
        {
            field: 'zip',
            title: 'Zip',
            width: 200
        }
    ];

    window.lead_columns = window.lead_columns.concat(<?= json_encode($additional_lead_columns) ?>);
    window.lead_columns = window.lead_columns.concat([
        {
            field: 'created_at',
            title: 'Created On',
            width: 200
        },
        {
            field: 'updated_at',
            title: 'Modified On',
            width: 200
        }
    ]);
</script>
<script src="/assets/js/user.leads.js"></script>