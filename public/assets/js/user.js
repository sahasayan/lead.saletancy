$(document).ready(function() {
    // init
    $('.datatable').DataTable();
    $('.menu .item').tab();
    $('#header-dropdown').dropdown();
});

function post_to_url(path, params, method = 'post')
{
    let form = document.createElement("form");
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for(let key in params) {
        if(params.hasOwnProperty(key)) {
            let hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
