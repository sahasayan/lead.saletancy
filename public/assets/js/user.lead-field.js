$(document).ready(function () {
    $('.ui.multiple.dropdown').dropdown({
        allowAdditions: true
    });
    $('.simple-dropdown').dropdown();
});

$('#add-lead-field').click(function () {
    $('#add-lead-field-modal').modal('show');
});

//
$('[name=field_type]').on('change', function () {
    let input = $(this);

    $('[name=field_show_as], [name=field_values]').closest('div.row').hide();

    if (['text', 'email', 'website', 'number'].indexOf(input.val()) >= 0) {
        $('[name=field_show_as]').closest('div.row').show();
    } else if (input.val() === 'dropdown') {
        $('[name=field_values]').closest('div.row').show();
    }
});