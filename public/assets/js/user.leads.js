$(document).ready(function() {
    $('#leads').kendoGrid({
        toolbar: ['excel', 'pdf'],
        excel: {
            fileName: 'leads.xlsx'
            //proxyURL: 'https://demos.telerik.com/kendo-ui/service/export',
            //filterable: true
        },
        pdf: {
            fileName: 'leads.pdf',
            allPages: true
        },
        dataSource: {
            data: window.leads
        },
        columns: window.lead_columns,
        columnMenu: true,
        // height: 'auto',
        scrollable: true,
        filterable: true,
        resizable: true,
        sortable: true,
        lockable: true,
        pageable: {
            pageSize: 5,
            pageSizes: [5, 10, 20, 50]
        },
        // groupable: true,
        // filterable: {
        //     mode: 'row'
        // },
        reorderable: true
        // navigatable: true,
        // selectable: true
    });
});

$('#add-new-lead').click(function () {
    $('#add-lead-modal').modal('show');
});

$('#add-lead-modal button[name=add_lead]').click(function () {

    // reset form errors
    $('#add-lead-modal .field.error').removeClass('error').find('span.color.red0').remove();

    $.ajax({
        method: 'POST',
        url: '/lead/add',
        data: $('#add-lead-modal form').serialize(),
        success: function (res) {
            console.table(res);
            if (res.is_validation_error === true) {
                $.each(res.errors, function (key, value) {
                        $('input[name=' + key + '], textarea[name=' + key + '], select[name=' + key +']')
                            .closest('.field')
                            .addClass('error')
                            .append('<span class="color red0">' + value + '</span>');
                });

            } else if (res.success === false) {
                swal({
                    type: 'error',
                    title: '',
                    text: res.message
                });

            } else if (res.success === true) {
                // reset form
                $('#add-lead-modal input, #add-lead-modal textarea').val('');
                swal({
                    type: 'success',
                    title: '',
                    text: 'added'
                });

            } else {
               console.log(res);
            }
       },
       error: function (err) {
           console.log(err);
       }
    });
});


$('input:text').click(function() {
    $(this).parent().find('input:file').click();
});

$('input:file', '.ui.action.input')
    .on('change', function(e) {
        var name = e.target.files[0].name;
        $('input:text', $(e.target).parent()).val(name);
    });